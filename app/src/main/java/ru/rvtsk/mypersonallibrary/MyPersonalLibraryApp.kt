package ru.rvtsk.mypersonallibrary

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyPersonalLibraryApp : Application()