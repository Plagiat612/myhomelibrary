package ru.rvtsk.mypersonallibrary.mappers

import ru.rvtsk.mypersonallibrary.domain.entities.Author
import ru.rvtsk.mypersonallibrary.entities.AuthorPresentation

fun Author.toPresentation(): AuthorPresentation {
    return AuthorPresentation(
        name = this.name,
        birthYear = this.birthYear,
        deathYear = this.deathYear
    )
}

fun AuthorPresentation.toAuthor(): Author {
    return Author(
        name = this.name,
        birthYear = this.birthYear,
        deathYear = this.deathYear
    )
}