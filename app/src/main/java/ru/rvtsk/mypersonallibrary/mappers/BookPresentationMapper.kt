package ru.rvtsk.mypersonallibrary.mappers

import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.entities.BookPresentation

fun Book.toPresentation(): BookPresentation {
    return BookPresentation(
        id = this.id,
        title = this.title,
        authors = this.authors.map { it.toPresentation() },
        translators = this.translators.map { it.toPresentation() },
        subjects = this.subjects,
        bookshelves = this.bookshelves,
        languages = this.languages,
        copyright = this.copyright,
        mediaType = this.mediaType,
        formats = this.formats,
        downloadCount = this.downloadCount
    )
}

fun BookPresentation.toBook(): Book {
    return Book(
        id = this.id,
        title = this.title,
        authors = this.authors.map { it.toAuthor() },
        translators = this.translators.map { it.toAuthor() },
        subjects = this.subjects,
        bookshelves = this.bookshelves,
        languages = this.languages,
        copyright = this.copyright,
        mediaType = this.mediaType,
        formats = this.formats,
        downloadCount = this.downloadCount
    )
}