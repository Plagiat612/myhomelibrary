package ru.rvtsk.mypersonallibrary.entities

data class BookPresentation(
    val id: Int,
    val title: String,
    val authors: List<AuthorPresentation>,
    val translators: List<AuthorPresentation>,
    val subjects: List<String>,
    val bookshelves: List<String>,
    val languages: List<String>,
    val copyright: Boolean,
    val mediaType: String,
    val formats: Map<String, String>,
    val downloadCount: Long
)
