package ru.rvtsk.mypersonallibrary.entities

data class AuthorPresentation(
    val name: String,
    val birthYear: Int? = null,
    val deathYear: Int? = null
)