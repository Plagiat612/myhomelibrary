package ru.rvtsk.mypersonallibrary.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "authors")
data class AuthorEntity(
    @PrimaryKey
    val name: String,
    val birthYear: Int?,
    val deathYear: Int?
)