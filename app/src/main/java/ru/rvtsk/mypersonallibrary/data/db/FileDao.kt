package ru.rvtsk.mypersonallibrary.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.rvtsk.mypersonallibrary.data.entities.FileEntity

@Dao
interface FileDao {
    @Query("SELECT * FROM books_storage WHERE bookId = :bookId")
    suspend fun getFilePaths(bookId: Int): FileEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePdfFilePath(fileEntity: FileEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveHtmlFilePath(fileEntity: FileEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveTextFilePath(fileEntity: FileEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveEpubFilePath(fileEntity: FileEntity)
}