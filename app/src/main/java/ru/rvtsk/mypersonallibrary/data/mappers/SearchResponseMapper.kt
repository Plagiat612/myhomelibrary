package ru.rvtsk.mypersonallibrary.data.mappers

import ru.rvtsk.mypersonallibrary.data.api.SearchResponse
import ru.rvtsk.mypersonallibrary.domain.entities.Author
import ru.rvtsk.mypersonallibrary.domain.entities.Book


fun SearchResponse.toBookList(): List<Book> {
    return this.results.map { book ->
        Book(
            id = book.id,
            title = book.title,
            authors = book.authors.map { it.toAuthor() },
            translators = book.translators.map { it.toAuthor() },
            subjects = book.subjects,
            bookshelves = book.bookshelves,
            languages = book.languages,
            copyright = book.copyright,
            mediaType = book.mediaType,
            formats = book.formats,
            downloadCount = book.downloadCount
        )
    }
}

fun ru.rvtsk.mypersonallibrary.data.api.Author.toAuthor(): Author {
    return Author(
        name = this.name,
        birthYear = this.birthYear,
        deathYear = this.deathYear
    )
}
