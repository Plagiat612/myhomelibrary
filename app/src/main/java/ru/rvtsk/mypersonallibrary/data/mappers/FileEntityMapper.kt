package ru.rvtsk.mypersonallibrary.data.mappers

import ru.rvtsk.mypersonallibrary.data.entities.FileEntity
import ru.rvtsk.mypersonallibrary.domain.entities.File

fun FileEntity?.toFile(): File {
    if (this == null) {
        return File(0, "", "", "", "")
    }
    return File(
        bookId = this.bookId,
        pdfPath = this.pdfPath,
        epubPath = this.epubPath,
        htmlPath = this.htmlPath,
        textPath = this.textPath
    )
}

fun File.toFileEntity() = FileEntity(
    bookId = this.bookId,
    pdfPath = this.pdfPath,
    epubPath = this.epubPath,
    htmlPath = this.htmlPath,
    textPath = this.textPath
)