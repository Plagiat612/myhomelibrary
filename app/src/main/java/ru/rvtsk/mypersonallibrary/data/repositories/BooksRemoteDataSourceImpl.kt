package ru.rvtsk.mypersonallibrary.data.repositories

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import ru.rvtsk.mypersonallibrary.data.api.GutendexSearchApi
import ru.rvtsk.mypersonallibrary.data.mappers.toBookList
import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class BooksRemoteDataSourceImpl @Inject constructor(
    private val gutendexSearchApi: GutendexSearchApi,
    private val okHttpClient: OkHttpClient
) : BooksRemoteDataSource {
    override suspend fun searchBooks(query: String): Result<List<Book>> =
        withContext(Dispatchers.IO) {
            try {
                val response = gutendexSearchApi.searchBooks(query)
                if (response.isSuccessful) {
                    return@withContext Result.Success(response.body()!!.toBookList())
                } else {
                    return@withContext Result.Error(Exception(response.message()))
                }
            } catch (e: Exception) {
                return@withContext Result.Error(e)
            }
        }

    override suspend fun downloadFile(url: String, destination: File): Boolean {
        return withContext(Dispatchers.IO) {
            try {
                val request = Request.Builder().url(url).build()
                val response = okHttpClient.newCall(request).execute()

                if (response.isSuccessful) {
                    response.body.let { responseBody ->
                        val inputStream = responseBody.byteStream()
                        val outputStream = FileOutputStream(destination)

                        val buffer = ByteArray(4096)
                        var bytesRead: Int
                        while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                            outputStream.write(buffer, 0, bytesRead)
                        }

                        outputStream.close()
                        inputStream.close()

                        return@withContext true
                    }
                }

                return@withContext false
            } catch (e: Exception) {
                return@withContext false
            }
        }
    }
}