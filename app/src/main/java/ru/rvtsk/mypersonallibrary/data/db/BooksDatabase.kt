package ru.rvtsk.mypersonallibrary.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity
import ru.rvtsk.mypersonallibrary.data.entities.BookEntity
import ru.rvtsk.mypersonallibrary.data.entities.FileEntity

@Database(
    entities = [BookEntity::class, AuthorEntity::class, FileEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class BooksDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDao
    abstract fun authorDao(): AuthorDao
    abstract fun fileDao(): FileDao

}