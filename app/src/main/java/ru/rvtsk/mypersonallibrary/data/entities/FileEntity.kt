package ru.rvtsk.mypersonallibrary.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "books_storage")
data class FileEntity(
    @PrimaryKey
    val bookId: Int,
    val pdfPath: String,
    val epubPath: String,
    val htmlPath: String,
    val textPath: String
)
