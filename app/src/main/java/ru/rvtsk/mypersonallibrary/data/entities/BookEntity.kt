package ru.rvtsk.mypersonallibrary.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "books")
data class BookEntity(
    @PrimaryKey
    val id: Int,
    val title: String,
    val authors: List<AuthorEntity>,
    val translators: List<AuthorEntity>,
    val subjects: List<String>,
    val bookshelves: List<String>,
    val languages: List<String>,
    val copyright: Boolean,
    val mediaType: String,
    val formats: Map<String, String>,
    val downloadCount: Long
)