package ru.rvtsk.mypersonallibrary.data.api

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Streaming

interface ProjectGutenbergApi {
    @GET("/ebooks/{query}")
    @Streaming
    suspend fun downloadBook(@Path("query") query: String): Response<ResponseBody>

}