package ru.rvtsk.mypersonallibrary.data.repositories

import ru.rvtsk.mypersonallibrary.data.mappers.toBook
import ru.rvtsk.mypersonallibrary.data.mappers.toBookEntity
import ru.rvtsk.mypersonallibrary.data.mappers.toFile
import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import java.io.File

class BooksRepositoryImpl(
    private val localDataSource: BooksLocalDataSource,
    private val remoteDataSource: BooksRemoteDataSource
) : BooksRepository {
    override suspend fun searchBooks(query: String): Result<List<Book>> {
        return remoteDataSource.searchBooks(query)
    }

    override suspend fun downloadBook(src: String, dst: File): Boolean {
        return remoteDataSource.downloadFile(src, dst)
    }

    override suspend fun saveBook(book: Book) {
        localDataSource.saveBook(book.toBookEntity())
    }

    override suspend fun loadBooks(): List<Book> {
        return localDataSource.getSavedBooks().map { it.toBook() }
    }

    override suspend fun loadBookById(id: Int): Book {
        return localDataSource.getSavedBookById(id).toBook()
    }

    override suspend fun saveTextFilePath(bookId: Int, path: String) {
        return localDataSource.saveTextFilePath(bookId, path)
    }

    override suspend fun getFilePaths(bookId: Int): ru.rvtsk.mypersonallibrary.domain.entities.File {
        return localDataSource.getFilePaths(bookId).toFile()
    }
}