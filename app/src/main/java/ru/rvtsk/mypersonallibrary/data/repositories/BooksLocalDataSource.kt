package ru.rvtsk.mypersonallibrary.data.repositories

import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity
import ru.rvtsk.mypersonallibrary.data.entities.BookEntity
import ru.rvtsk.mypersonallibrary.data.entities.FileEntity

interface BooksLocalDataSource {
    suspend fun saveBook(book: BookEntity)
    suspend fun getSavedBooks(): List<BookEntity>
    suspend fun getSavedBookById(id: Int): BookEntity
    suspend fun getSavedBooksByTitle(title: String): List<BookEntity>
    suspend fun deleteBook(book: BookEntity)

    suspend fun saveAuthor(author: AuthorEntity)
    suspend fun getSavedAuthors(): List<AuthorEntity>
    suspend fun getSavedAuthorByName(name: String): AuthorEntity
    suspend fun deleteAuthor(author: AuthorEntity)

    suspend fun getFilePaths(bookId: Int): FileEntity
    suspend fun savePdfFilePath(bookId: Int, path: String)
    suspend fun saveHtmlFilePath(bookId: Int, path: String)
    suspend fun saveTextFilePath(bookId: Int, path: String)
    suspend fun saveEpubFilePath(bookId: Int, path: String)
}