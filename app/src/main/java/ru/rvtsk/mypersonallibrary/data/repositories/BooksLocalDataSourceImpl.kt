package ru.rvtsk.mypersonallibrary.data.repositories

import ru.rvtsk.mypersonallibrary.data.db.AuthorDao
import ru.rvtsk.mypersonallibrary.data.db.BookDao
import ru.rvtsk.mypersonallibrary.data.db.FileDao
import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity
import ru.rvtsk.mypersonallibrary.data.entities.BookEntity
import ru.rvtsk.mypersonallibrary.data.entities.FileEntity
import javax.inject.Inject

class BooksLocalDataSourceImpl @Inject constructor(
    private val bookDao: BookDao,
    private val authorDao: AuthorDao,
    private val fileDao: FileDao
) : BooksLocalDataSource {
    override suspend fun saveBook(book: BookEntity) {
        bookDao.saveBook(book)
    }

    override suspend fun getSavedBooks(): List<BookEntity> {
        return bookDao.getSavedBooks()
    }

    override suspend fun getSavedBookById(id: Int): BookEntity {
        return bookDao.getSavedBookById(id)
    }

    override suspend fun getSavedBooksByTitle(title: String): List<BookEntity> {
        return bookDao.getSavedBooksByTitle(title)
    }

    override suspend fun deleteBook(book: BookEntity) {
        bookDao.deleteBook(book)
    }

    override suspend fun saveAuthor(author: AuthorEntity) {
        authorDao.saveAuthor(author)
    }

    override suspend fun getSavedAuthors(): List<AuthorEntity> {
        return authorDao.getSavedAuthors()
    }

    override suspend fun getSavedAuthorByName(name: String): AuthorEntity {
        return authorDao.getSavedAuthorByName(name)
    }

    override suspend fun deleteAuthor(author: AuthorEntity) {
        authorDao.deleteAuthor(author)
    }

    override suspend fun getFilePaths(bookId: Int): FileEntity {
        return fileDao.getFilePaths(bookId)
    }

    override suspend fun savePdfFilePath(bookId: Int, path: String) {
        val fileEntity = FileEntity(bookId, pdfPath = path, "", "", "")
        fileDao.savePdfFilePath(fileEntity)
    }

    override suspend fun saveHtmlFilePath(bookId: Int, path: String) {
        val fileEntity = FileEntity(bookId, "", "", htmlPath = path, "")
        fileDao.saveHtmlFilePath(fileEntity)
    }

    override suspend fun saveTextFilePath(bookId: Int, path: String) {
        val fileEntity = FileEntity(bookId, "", "", "", textPath = path)
        fileDao.saveTextFilePath(fileEntity)
    }

    override suspend fun saveEpubFilePath(bookId: Int, path: String) {
        val fileEntity = FileEntity(bookId, "", epubPath = path, "", "")
        fileDao.saveEpubFilePath(fileEntity)
    }

}