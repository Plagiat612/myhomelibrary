package ru.rvtsk.mypersonallibrary.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity

@Dao
interface AuthorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAuthor(author: AuthorEntity)

    @Query("SELECT * FROM authors")
    suspend fun getSavedAuthors(): List<AuthorEntity>

    @Query("SELECT * FROM authors WHERE name LIKE :name")
    suspend fun getSavedAuthorByName(name: String): AuthorEntity

    @Delete
    suspend fun deleteAuthor(author: AuthorEntity)
}
