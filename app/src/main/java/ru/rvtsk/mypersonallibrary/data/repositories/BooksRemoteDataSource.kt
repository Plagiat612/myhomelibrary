package ru.rvtsk.mypersonallibrary.data.repositories

import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import java.io.File

interface BooksRemoteDataSource {
    suspend fun searchBooks(query: String): Result<List<Book>>
    suspend fun downloadFile(src: String, dst: File): Boolean
}