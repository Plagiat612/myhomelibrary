package ru.rvtsk.mypersonallibrary.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.rvtsk.mypersonallibrary.data.entities.BookEntity

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveBook(book: BookEntity)

    @Query("SELECT * FROM books")
    suspend fun getSavedBooks(): List<BookEntity>

    @Query("SELECT * FROM books WHERE id LIKE :id")
    suspend fun getSavedBookById(id: Int): BookEntity

    @Query("SELECT * FROM books WHERE title LIKE :title")
    suspend fun getSavedBooksByTitle(title: String): List<BookEntity>

    @Delete
    suspend fun deleteBook(book: BookEntity)
}

