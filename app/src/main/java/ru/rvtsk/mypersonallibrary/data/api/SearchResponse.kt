package ru.rvtsk.mypersonallibrary.data.api

import com.squareup.moshi.Json

data class SearchResponse(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<Book>
)

data class Book(
    val id: Int,
    val title: String,
    val authors: List<Author>,
    val translators: List<Author>,
    val subjects: List<String>,
    val bookshelves: List<String>,
    val languages: List<String>,
    val copyright: Boolean,
    @Json(name = "media_type")
    val mediaType: String,
    val formats: Map<String, String>,
    @Json(name = "download_count")
    val downloadCount: Long
)

data class Author(
    val name: String,
    @Json(name = "birth_year")
    val birthYear: Int?,
    @Json(name = "death_year")
    val deathYear: Int?
)