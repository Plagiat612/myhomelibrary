package ru.rvtsk.mypersonallibrary.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity
import javax.inject.Inject

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun fromAuthorList(authorList: List<AuthorEntity>): String {
        return gson.toJson(authorList)
    }

    @TypeConverter
    fun toAuthorList(authorListString: String): List<AuthorEntity> {
        val type = object : TypeToken<List<AuthorEntity>>() {}.type
        return gson.fromJson(authorListString, type)
    }

    @TypeConverter
    fun fromStringList(stringList: List<String>): String {
        return gson.toJson(stringList)
    }

    @TypeConverter
    fun toStringList(stringListString: String): List<String> {
        val type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(stringListString, type)
    }

    @TypeConverter
    fun fromFormatMap(formatMap: Map<String, String>): String {
        return gson.toJson(formatMap)
    }

    @TypeConverter
    fun toFormatMap(formatMapString: String): Map<String, String> {
        val type = object : TypeToken<Map<String, String>>() {}.type
        return gson.fromJson(formatMapString, type)
    }
}