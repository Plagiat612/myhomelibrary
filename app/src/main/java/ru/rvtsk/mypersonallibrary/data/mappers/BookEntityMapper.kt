package ru.rvtsk.mypersonallibrary.data.mappers

import ru.rvtsk.mypersonallibrary.data.entities.BookEntity
import ru.rvtsk.mypersonallibrary.domain.entities.Book


fun Book.toBookEntity(): BookEntity {
    return BookEntity(
        id = this.id,
        title = this.title,
        authors = this.authors.map { it.toAuthorEntity() },
        translators = this.translators.map { it.toAuthorEntity() },
        subjects = this.subjects,
        bookshelves = this.bookshelves,
        languages = this.languages,
        copyright = this.copyright,
        mediaType = this.mediaType,
        formats = this.formats,
        downloadCount = this.downloadCount
    )
}

fun BookEntity.toBook(): Book {
    return Book(
        id = this.id,
        title = this.title,
        authors = this.authors.map { it.toAuthor() },
        translators = this.translators.map { it.toAuthor() },
        subjects = this.subjects,
        bookshelves = this.bookshelves,
        languages = this.languages,
        copyright = this.copyright,
        mediaType = this.mediaType,
        formats = this.formats,
        downloadCount = this.downloadCount
    )
}
