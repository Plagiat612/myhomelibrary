package ru.rvtsk.mypersonallibrary.data.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GutendexSearchApi {
    @GET("/books")
    suspend fun searchBooks(
        @Query("search") query: String,
        @Query("page") page: Int = 1,
        @Query("page_size") pageSize: Int = 10
    ): Response<SearchResponse>
}