package ru.rvtsk.mypersonallibrary.data.mappers

import ru.rvtsk.mypersonallibrary.data.entities.AuthorEntity
import ru.rvtsk.mypersonallibrary.domain.entities.Author

fun Author.toAuthorEntity(): AuthorEntity {
    return AuthorEntity(
        name = this.name,
        birthYear = this.birthYear,
        deathYear = this.deathYear
    )
}

fun AuthorEntity.toAuthor(): Author {
    return Author(
        name = this.name,
        birthYear = this.birthYear,
        deathYear = this.deathYear
    )
}
