package ru.rvtsk.mypersonallibrary.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.rvtsk.mypersonallibrary.data.repositories.BooksLocalDataSource
import ru.rvtsk.mypersonallibrary.data.repositories.BooksRemoteDataSource
import ru.rvtsk.mypersonallibrary.data.repositories.BooksRepositoryImpl
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository

@Module
@InstallIn(SingletonComponent::class)
class BooksRepositoryModule {

    @Provides
    fun provideBooksRepository(
        localDataSource: BooksLocalDataSource,
        remoteDataSource: BooksRemoteDataSource
    ): BooksRepository = BooksRepositoryImpl(
        localDataSource, remoteDataSource
    )

}