package ru.rvtsk.mypersonallibrary.di

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ResourceProvider @Inject constructor(
    @ApplicationContext
    private val appContext: Context,
) {
    operator fun invoke() = appContext

    fun getString(id: Int): String {
        return appContext.getString(id)
    }
}