package ru.rvtsk.mypersonallibrary.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.rvtsk.mypersonallibrary.data.db.BooksDatabase

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    fun provideBooksDatabase(
        @ApplicationContext
        context: Context
    ) = Room.databaseBuilder(
        context,
        BooksDatabase::class.java,
        "booksdb"
    ).build()

    @Provides
    fun provideBookDao(
        booksDatabase: BooksDatabase
    ) = booksDatabase.bookDao()

    @Provides
    fun provideAuthorDao(
        booksDatabase: BooksDatabase
    ) = booksDatabase.authorDao()

    @Provides
    fun provideFileDao(
        booksDatabase: BooksDatabase
    ) = booksDatabase.fileDao()
}