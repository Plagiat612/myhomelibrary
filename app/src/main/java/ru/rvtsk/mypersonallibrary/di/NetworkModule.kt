@file:Suppress("DEPRECATION")

package ru.rvtsk.mypersonallibrary.di

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import ru.rvtsk.mypersonallibrary.R
import ru.rvtsk.mypersonallibrary.data.api.GutendexSearchApi
import ru.rvtsk.mypersonallibrary.data.api.ProjectGutenbergApi
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    @Provides
    fun provideSearchApi(
        builder: Retrofit.Builder
    ): GutendexSearchApi = builder.build().create(GutendexSearchApi::class.java)

    @Provides
    fun provideRetrofitSearch(
        @ApplicationContext context: Context,
        moshi: Moshi,
        okHttpClient: OkHttpClient
    ): Retrofit.Builder = Retrofit.Builder()
        .baseUrl(context.getString(R.string.api_host))
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))

    @Provides
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
}