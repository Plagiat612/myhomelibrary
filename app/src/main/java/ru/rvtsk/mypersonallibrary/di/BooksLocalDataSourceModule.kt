package ru.rvtsk.mypersonallibrary.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.rvtsk.mypersonallibrary.data.db.AuthorDao
import ru.rvtsk.mypersonallibrary.data.db.BookDao
import ru.rvtsk.mypersonallibrary.data.db.FileDao
import ru.rvtsk.mypersonallibrary.data.repositories.BooksLocalDataSource
import ru.rvtsk.mypersonallibrary.data.repositories.BooksLocalDataSourceImpl

@Module
@InstallIn(SingletonComponent::class)
class BooksLocalDataSourceModule {
    @Provides
    fun provideBooksLocalDataSource(
        bookDao: BookDao,
        authorDao: AuthorDao,
        fileDao: FileDao
    ): BooksLocalDataSource = BooksLocalDataSourceImpl(
        bookDao, authorDao, fileDao
    )
}