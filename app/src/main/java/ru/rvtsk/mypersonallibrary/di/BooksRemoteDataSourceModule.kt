package ru.rvtsk.mypersonallibrary.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import ru.rvtsk.mypersonallibrary.data.api.GutendexSearchApi
import ru.rvtsk.mypersonallibrary.data.repositories.BooksRemoteDataSource
import ru.rvtsk.mypersonallibrary.data.repositories.BooksRemoteDataSourceImpl

@Module
@InstallIn(SingletonComponent::class)
class BooksRemoteDataSourceModule {
    @Provides
    fun provideBooksRemoteDataSource(
        gutendexSearchApi: GutendexSearchApi,
        okHttpClient: OkHttpClient
    ): BooksRemoteDataSource = BooksRemoteDataSourceImpl(
        gutendexSearchApi, okHttpClient
    )
}