package ru.rvtsk.mypersonallibrary.domain.entities

data class Author(
    val name: String,
    val birthYear: Int? = null,
    val deathYear: Int? = null
)
