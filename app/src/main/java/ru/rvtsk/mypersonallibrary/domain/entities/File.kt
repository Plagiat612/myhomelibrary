package ru.rvtsk.mypersonallibrary.domain.entities

data class File(
    val bookId: Int,
    val pdfPath: String,
    val epubPath: String,
    val htmlPath: String,
    val textPath: String
)