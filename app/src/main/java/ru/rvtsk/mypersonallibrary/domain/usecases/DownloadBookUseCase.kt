package ru.rvtsk.mypersonallibrary.domain.usecases

import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import java.io.File
import javax.inject.Inject


class DownloadBookUseCase @Inject constructor(
    private val booksRepository: BooksRepository
) {
    suspend operator fun invoke(url: String, dst: File): Boolean {
        return booksRepository.downloadBook(url, dst)
    }
}