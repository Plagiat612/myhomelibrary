package ru.rvtsk.mypersonallibrary.domain.usecases

import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import javax.inject.Inject

class LoadBooksUseCase @Inject constructor(
    private val booksRepository: BooksRepository
) {
    suspend operator fun invoke(): List<Book> = booksRepository.loadBooks()
}