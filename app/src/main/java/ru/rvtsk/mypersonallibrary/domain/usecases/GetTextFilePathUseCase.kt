package ru.rvtsk.mypersonallibrary.domain.usecases

import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import javax.inject.Inject

class GetTextFilePathUseCase @Inject constructor(
    private val booksRepository: BooksRepository
) {
    suspend operator fun invoke(bookId: Int): String {
        return booksRepository.getFilePaths(bookId).textPath
    }
}