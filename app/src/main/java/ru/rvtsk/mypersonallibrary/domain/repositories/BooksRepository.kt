package ru.rvtsk.mypersonallibrary.domain.repositories

import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import java.io.File

interface BooksRepository {
    suspend fun searchBooks(query: String): Result<List<Book>>

    suspend fun downloadBook(src: String, dst: File): Boolean
    suspend fun saveBook(book: Book)
    suspend fun loadBooks(): List<Book>
    suspend fun loadBookById(id: Int): Book

    suspend fun saveTextFilePath(bookId: Int, path: String)
    suspend fun getFilePaths(bookId: Int): ru.rvtsk.mypersonallibrary.domain.entities.File
}
