package ru.rvtsk.mypersonallibrary.domain.usecases

import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import javax.inject.Inject

class LoadBookByIdUseCase @Inject constructor(
    private val booksRepository: BooksRepository
) {
    suspend operator fun invoke(id: Int): Book = booksRepository.loadBookById(id)

}