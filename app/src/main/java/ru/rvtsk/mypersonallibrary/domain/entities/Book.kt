package ru.rvtsk.mypersonallibrary.domain.entities

data class Book(
    val id: Int,
    val title: String,
    val authors: List<Author>,
    val translators: List<Author>,
    val subjects: List<String>,
    val bookshelves: List<String>,
    val languages: List<String>,
    val copyright: Boolean,
    val mediaType: String,
    val formats: Map<String, String>,
    val downloadCount: Long
)
