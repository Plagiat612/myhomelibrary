package ru.rvtsk.mypersonallibrary.domain.usecases

import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import javax.inject.Inject

class SaveTextFilePathUseCase @Inject constructor(
    private val booksRepository: BooksRepository
) {
    suspend operator fun invoke(bookId: Int, path: String) {
        booksRepository.saveTextFilePath(bookId, path)
    }
}