package ru.rvtsk.mypersonallibrary.presentation.reader

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.rvtsk.mypersonallibrary.domain.usecases.GetTextFilePathUseCase
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ReaderViewModel @Inject constructor(
    private val getTextFilePathUseCase: GetTextFilePathUseCase
) : ViewModel() {

    private val _currentPage = MutableStateFlow<Int>(0)
    val currentPage: MutableStateFlow<Int>
        get() = _currentPage

    private val _pages = MutableStateFlow<List<String>>(emptyList())
    val pages: MutableStateFlow<List<String>>
        get() = _pages

    private val _error = MutableStateFlow<String>("")
    val error: MutableStateFlow<String>
        get() = _error


    private var totalPages: Int = 0
    private val linesPerPage: Int = 22

    fun loadAndSplitTextFromFile(bookId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val file = getTextFilePathUseCase(bookId)?.let { File(it) }
                val reader = BufferedReader(FileReader(file))
                val lines = mutableListOf<String>()
                var line: String? = reader.readLine()
                while (line != null) {
                    lines.add(line)
                    line = reader.readLine()
                }
                reader.close()

                totalPages = (lines.size + linesPerPage - 1) / linesPerPage

                val pages = mutableListOf<String>()
                for (i in 0 until totalPages) {
                    val startIndex = i * linesPerPage
                    val endIndex = minOf(startIndex + linesPerPage, lines.size)
                    val pageLines = lines.subList(startIndex, endIndex)
                    val pageText = pageLines.joinToString("\n")
                    pages.add(pageText)
                }

                withContext(Dispatchers.Main) {
                    _pages.value = pages
                    _currentPage.value = 0
                }
            } catch (e: IOException) {
                _error.value = e.localizedMessage.orEmpty()
            }
        }
    }

    fun goToPreviousPage() {
        val currentPage = currentPage.value ?: 0
        if (currentPage > 0) {
            _currentPage.value = currentPage - 1
        }
    }

    fun goToNextPage() {
        val currentPage = currentPage.value ?: 0
        if (currentPage < totalPages - 1) {
            _currentPage.value = currentPage + 1
        }
    }
}
