package ru.rvtsk.mypersonallibrary.presentation.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.usecases.SaveBookUseCase
import ru.rvtsk.mypersonallibrary.domain.usecases.SearchBooksUseCase
import ru.rvtsk.mypersonallibrary.entities.BookPresentation
import ru.rvtsk.mypersonallibrary.mappers.toBook
import ru.rvtsk.mypersonallibrary.mappers.toPresentation
import javax.inject.Inject


@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchBooksUseCase: SearchBooksUseCase,
    private val saveBookUseCase: SaveBookUseCase
) : ViewModel() {

    private val _dataLoading = MutableStateFlow(false)
    val dataLoading: StateFlow<Boolean> = _dataLoading

    private val _books = MutableStateFlow<List<BookPresentation>>(emptyList())
    val books: StateFlow<List<BookPresentation>> = _books

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun searchBooks(query: String) {
        viewModelScope.launch {
            _dataLoading.value = true
            when (val booksResult = searchBooksUseCase.invoke(query)) {
                is Result.Success -> {
                    _books.value = booksResult.data.map { it.toPresentation() }
                }

                is Result.Error -> {
                    _books.value = emptyList()
                    _error.value = booksResult.exception.localizedMessage.orEmpty()
                }
            }
            _dataLoading.value = false
        }
    }

    fun saveBook(book: BookPresentation) = viewModelScope.launch {
        _dataLoading.value = true
        saveBookUseCase(book.toBook())
        _dataLoading.value = false
    }
}
