package ru.rvtsk.mypersonallibrary.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import ru.rvtsk.mypersonallibrary.R
import ru.rvtsk.mypersonallibrary.presentation.book.BookScreen
import ru.rvtsk.mypersonallibrary.presentation.book.BookViewModel
import ru.rvtsk.mypersonallibrary.presentation.bookshelf.BookshelfScreen
import ru.rvtsk.mypersonallibrary.presentation.bookshelf.BookshelfViewModel
import ru.rvtsk.mypersonallibrary.presentation.reader.ReaderScreen
import ru.rvtsk.mypersonallibrary.presentation.reader.ReaderViewModel
import ru.rvtsk.mypersonallibrary.presentation.search.SearchScreen
import ru.rvtsk.mypersonallibrary.presentation.search.SearchViewModel
import ru.rvtsk.mypersonallibrary.ui.theme.MyPersonalLibraryTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyPersonalLibraryTheme {
                val navController = rememberNavController()
                val snackbarHostState = remember { SnackbarHostState() }
                val showFloatingActionButton by lazy { mutableStateOf(false) }
                Scaffold(
                    snackbarHost = { SnackbarHost(snackbarHostState) },
                    floatingActionButton = {
                        if (showFloatingActionButton.value) {
                            FloatingActionButton(
                                onClick = { navController.navigate("search") },
                                modifier = Modifier.border(
                                    1.dp,
                                    color = MaterialTheme.colorScheme.onBackground,
                                    shape = MaterialTheme.shapes.small
                                )
                            ) {
                                Icon(
                                    Icons.Default.Search,
                                    contentDescription = getString(R.string.search_label)
                                )
                            }
                        }
                    }
                ) { paddingValues ->
                    NavigationGraph(
                        navController = navController,
                        modifier = Modifier
                            .padding(paddingValues)
                            .padding(5.dp),
                        snackbarHostState = snackbarHostState,
                        onBookshelfScreen = { showFloatingActionButton.value = true },
                        onOtherScreen = { showFloatingActionButton.value = false }
                    )
                }
            }
        }
    }
}

@Composable
fun NavigationGraph(
    navController: NavHostController,
    modifier: Modifier,
    snackbarHostState: SnackbarHostState,
    onBookshelfScreen: () -> Unit,
    onOtherScreen: () -> Unit
) {
    NavHost(navController, startDestination = "bookshelf") {
        composable("bookshelf") {
            val viewModel = hiltViewModel<BookshelfViewModel>()
            onBookshelfScreen()
            BookshelfScreen(
                viewModel,
                modifier,
                snackbarHostState,
                onClick = { navController.navigate("books/${it.id}") })
        }
        composable(
            "books/{id}",
            arguments = listOf(navArgument("id") { type = NavType.StringType })
        ) { backStackEntry ->
            val viewModel = hiltViewModel<BookViewModel>()
            onOtherScreen()
            backStackEntry.arguments?.getString("id")?.let { id ->
                BookScreen(
                    viewModel,
                    modifier,
                    snackbarHostState,
                    onReadClick = { navController.navigate("reader/${id}") },
                    id.toInt()
                )
            }
        }
        composable(
            "reader/{id}",
            arguments = listOf(navArgument("id") { type = NavType.StringType })
        ) {
            val viewModel = hiltViewModel<ReaderViewModel>()
            onOtherScreen()
            ReaderScreen(viewModel, modifier, snackbarHostState, navController)
        }
        composable("search") {
            val viewModel = hiltViewModel<SearchViewModel>()
            onOtherScreen()
            SearchScreen(
                viewModel,
                modifier,
                snackbarHostState,
                onClick = { navController.navigate("books/${it.id}") }
            )
        }
    }
}
