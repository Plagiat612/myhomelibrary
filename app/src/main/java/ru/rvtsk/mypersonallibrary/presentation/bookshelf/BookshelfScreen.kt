package ru.rvtsk.mypersonallibrary.presentation.bookshelf

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.rvtsk.mypersonallibrary.entities.BookPresentation

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookshelfScreen(
    viewModel: BookshelfViewModel,
    modifier: Modifier,
    snackbarHostState: SnackbarHostState,
    onClick: (book: BookPresentation) -> Unit
) {
    val books by viewModel.books.collectAsState()
    val isLoading by viewModel.dataLoading.collectAsState(false)
    val error by viewModel.error.collectAsState()

    LaunchedEffect(error) {
        if (error.isNotEmpty()) snackbarHostState.showSnackbar(error)
    }

    LaunchedEffect(books) {
        viewModel.loadBooks()
    }

    Column(modifier = modifier) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            if (isLoading) {
                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
            } else {
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(books) { book ->
                        BookItemCard(
                            book = book,
                            modifier = Modifier.padding(5.dp),
                            onClick = { onClick(book) }
                        )
                    }
                }
            }
        }

    }
}
