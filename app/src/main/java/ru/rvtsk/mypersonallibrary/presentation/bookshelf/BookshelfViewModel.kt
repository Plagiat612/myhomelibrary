package ru.rvtsk.mypersonallibrary.presentation.bookshelf

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.rvtsk.mypersonallibrary.domain.usecases.LoadBooksUseCase
import ru.rvtsk.mypersonallibrary.entities.BookPresentation
import ru.rvtsk.mypersonallibrary.mappers.toPresentation
import javax.inject.Inject


@HiltViewModel
class BookshelfViewModel @Inject constructor(
    private val loadBooksUseCase: LoadBooksUseCase
) : ViewModel() {

    private val _dataLoading = MutableStateFlow(false)
    val dataLoading: StateFlow<Boolean> = _dataLoading

    private val _books = MutableStateFlow<List<BookPresentation>>(emptyList())
    val books: StateFlow<List<BookPresentation>> = _books

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun loadBooks() {
        viewModelScope.launch {
            _dataLoading.value = true
            _books.value = loadBooksUseCase().map { it.toPresentation() }
            _dataLoading.value = false
        }
    }

    init {
        loadBooks()
    }
}
