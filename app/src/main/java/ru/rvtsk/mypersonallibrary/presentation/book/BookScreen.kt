package ru.rvtsk.mypersonallibrary.presentation.book

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import ru.rvtsk.mypersonallibrary.R

@Composable
fun BookScreen(
    viewModel: BookViewModel,
    modifier: Modifier,
    snackbarHostState: SnackbarHostState,
    onReadClick: (id: Int) -> Unit,
    id: Int
) {
    val book by viewModel.book.collectAsState()
    val isLoading by viewModel.dataLoading.collectAsState(false)
    val completed by viewModel.completed.collectAsState()
    val error by viewModel.error.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(error) {
        if (error.isNotEmpty()) snackbarHostState.showSnackbar(error)
    }

    LaunchedEffect(id) {
        viewModel.loadBookById(id)
    }

    val coroutineScope = rememberCoroutineScope()

    Column(modifier = modifier) {
        book?.let {
            Text(text = it.title)
            LazyColumn {
                items(it.authors) { author ->
                    Text(
                        text = stringResource(
                            R.string.author_label,
                            author.name,
                            author.birthYear ?: "",
                            author.deathYear ?: ""
                        )
                    )
                }
                items(it.translators) { author ->
                    Text(
                        text = stringResource(
                            R.string.translator_label,
                            author.name,
                            author.birthYear ?: "",
                            author.deathYear ?: ""
                        )
                    )
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = {
                    when (completed) {
                        true -> onReadClick(it.id)
                        false -> {
                            try {
                                val pattern = "text/plain.*".toRegex()
                                val url = it.formats.filterKeys { key ->
                                    pattern.matches(key)
                                }.values.first()
                                viewModel.downloadBook(it.id, url)
                            } catch (e: NoSuchElementException) {
                                coroutineScope.launch {
                                    snackbarHostState.showSnackbar(context.getString(R.string.not_available_now))
                                }
                            }
                        }
                    }
                }
            ) {
                if (isLoading) {
                    CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterVertically))
                } else {
                    if (completed) {
                        Text(text = stringResource(R.string.read_btn))
                    } else {
                        Text(text = stringResource(R.string.download_btn))
                    }
                }
            }
        }
    }
}