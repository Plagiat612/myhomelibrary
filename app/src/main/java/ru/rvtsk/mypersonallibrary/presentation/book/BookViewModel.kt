package ru.rvtsk.mypersonallibrary.presentation.book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.rvtsk.mypersonallibrary.R
import ru.rvtsk.mypersonallibrary.di.ResourceProvider
import ru.rvtsk.mypersonallibrary.domain.usecases.DownloadBookUseCase
import ru.rvtsk.mypersonallibrary.domain.usecases.GetTextFilePathUseCase
import ru.rvtsk.mypersonallibrary.domain.usecases.LoadBookByIdUseCase
import ru.rvtsk.mypersonallibrary.domain.usecases.SaveTextFilePathUseCase
import ru.rvtsk.mypersonallibrary.entities.BookPresentation
import ru.rvtsk.mypersonallibrary.mappers.toPresentation
import java.io.File
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val loadBookByIdUseCase: LoadBookByIdUseCase,
    private val downloadBookUseCase: DownloadBookUseCase,
    private val saveTextFilePathUseCase: SaveTextFilePathUseCase,
    private val getTextFilePathUseCase: GetTextFilePathUseCase
) : ViewModel() {
    private val _dataLoading = MutableStateFlow(false)
    val dataLoading: StateFlow<Boolean> = _dataLoading

    private val _completed = MutableStateFlow(false)
    val completed: StateFlow<Boolean> = _completed

    private val _book: MutableStateFlow<BookPresentation?> = MutableStateFlow(null)
    val book: StateFlow<BookPresentation?> = _book

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun downloadBook(bookId: Int, url: String) {
        val dst = File(resourceProvider().filesDir.path, url.hashCode().toString())
        viewModelScope.launch {
            _dataLoading.value = true
            val success = downloadBookUseCase(url, dst)
            if (success) {
                saveTextFilePathUseCase(bookId, dst.absolutePath)
                _completed.value = true
            } else {
                _error.value = resourceProvider.getString(R.string.file_not_downloaded)
            }
            _dataLoading.value = false
        }
    }

    fun loadBookById(id: Int) {
        viewModelScope.launch {
            _dataLoading.value = true
            _book.value = loadBookByIdUseCase(id).toPresentation()
            _completed.value = File(getTextFilePathUseCase(id)).exists() == true
            _dataLoading.value = false
        }
    }
}