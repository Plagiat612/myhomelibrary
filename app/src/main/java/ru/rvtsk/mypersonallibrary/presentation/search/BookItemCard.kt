package ru.rvtsk.mypersonallibrary.presentation.search

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults.cardElevation
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.rvtsk.mypersonallibrary.R
import ru.rvtsk.mypersonallibrary.entities.AuthorPresentation
import ru.rvtsk.mypersonallibrary.entities.BookPresentation

@Composable
fun BookItemCard(
    book: BookPresentation,
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {}
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(100.dp)
            .wrapContentHeight()
            .clickable(onClick = onClick),
        elevation = cardElevation(4.dp)
    ) {
        Spacer(modifier = Modifier.width(16.dp))
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = book.title,
                style = MaterialTheme.typography.titleLarge,
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = book.authors.joinToString(";") { it.name },
                style = MaterialTheme.typography.bodyMedium,
            )
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                modifier = Modifier.fillMaxWidth().padding(4.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = book.languages.toString(),
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.width(4.dp))
                Text(
                    text = book.bookshelves.firstOrNull() ?: "",
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = stringResource(R.string.downloads_label, book.downloadCount)
                )
            }
        }
    }
}

@Composable
@Preview(name = "BookItemCard")
fun BookItemCardPreview() {
    val exampleBook = BookPresentation(
        id = 12345,
        title = "War and Peace. Vol 1",
        authors = listOf(AuthorPresentation("Lev Tolstoy", 1828, 1910)),
        translators = listOf(),
        subjects = listOf(),
        bookshelves = listOf(),
        languages = listOf(),
        copyright = false,
        mediaType = "Text",
        formats = mapOf("text/plain" to "link"),
        downloadCount = 1000000
    )
    BookItemCard(book = exampleBook)
}

