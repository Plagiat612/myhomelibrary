package ru.rvtsk.mypersonallibrary.presentation.reader

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import ru.rvtsk.mypersonallibrary.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReaderScreen(
    viewModel: ReaderViewModel,
    modifier: Modifier,
    snackbarHostState: SnackbarHostState,
    navController: NavHostController
) {
    //val bookText by viewModel.bookText.collectAsState()
    //val isLoading by viewModel.dataLoading.collectAsState(false)
    val error by viewModel.error.collectAsState("")
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val bookId = navBackStackEntry?.arguments?.getString("id")?.toInt()
    val pages by viewModel.pages.collectAsState(emptyList())
    val currentPage by viewModel.currentPage.collectAsState(0)

    LaunchedEffect(bookId) {
        if (bookId != null) {
            viewModel.loadAndSplitTextFromFile(bookId)
        }
    }

    Column(modifier = modifier) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f)
        ) {
            if (pages.isNotEmpty()) {
                val currentText = pages.getOrNull(currentPage) ?: ""
                Text(
                    text = currentText,
                    modifier = Modifier.verticalScroll(rememberScrollState())
                )
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.4f),
                onClick = { viewModel.goToPreviousPage() },
                enabled = currentPage > 0
            ) {
                Text(stringResource(R.string.previous_btn))
            }
            Text(
                text = currentPage.toString(),
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.2f),
                textAlign = TextAlign.Center
            )
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.4f),
                onClick = { viewModel.goToNextPage() },
                enabled = currentPage < (pages.size - 1)
            ) {
                Text(stringResource(R.string.next_btn))
            }
        }
    }

}
