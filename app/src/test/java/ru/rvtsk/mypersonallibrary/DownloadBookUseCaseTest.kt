package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.DownloadBookUseCase
import java.io.File

@RunWith(MockitoJUnitRunner::class)
class DownloadBookUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var downloadBookUseCase: DownloadBookUseCase

    @Before
    fun setup(){
        downloadBookUseCase = DownloadBookUseCase(booksRepository)
    }

    @Test
    fun `invoke should download book successfully`() = runTest {
        // Arrange
        val url = "https://example.com/book"
        val dst = File("path/to/save/book.pdf")
        `when`(booksRepository.downloadBook(url, dst)).thenReturn(true)

        // Act
        val result = downloadBookUseCase(url, dst)

        // Assert
        assertTrue(result)
        verify(booksRepository).downloadBook(url, dst)
    }

    @Test
    fun `invoke should return false if book download fails`() = runTest {
        // Arrange
        val url = "https://example.com/book"
        val dst = File("path/to/save/book.pdf")
        `when`(booksRepository.downloadBook(url, dst)).thenReturn(false)

        // Act
        val result = downloadBookUseCase(url, dst)

        // Assert
        assertTrue(!result)
        verify(booksRepository).downloadBook(url, dst)
    }
}