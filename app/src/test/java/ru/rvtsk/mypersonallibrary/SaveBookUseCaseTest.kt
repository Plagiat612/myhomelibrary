package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.SaveBookUseCase

@RunWith(MockitoJUnitRunner::class)
class SaveBookUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var saveBookUseCase: SaveBookUseCase

    @Before
    fun setup(){
        saveBookUseCase = SaveBookUseCase(booksRepository)
    }

    @Test
    fun `invoke should save book`() = runTest {
        // Arrange
        val book = Book(id = 123, title = "Sample Book", emptyList(), emptyList(), emptyList(), emptyList(),
            emptyList(), false,"Text", mapOf("" to ""), 14)


        // Act
        saveBookUseCase(book)

        // Assert
        verify(booksRepository).saveBook(book)
    }
}
