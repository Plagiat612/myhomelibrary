package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.LoadBookByIdUseCase

@RunWith(MockitoJUnitRunner::class)
class LoadBookByIdUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var loadBookByIdUseCase: LoadBookByIdUseCase

    @Before
    fun setup(){
       loadBookByIdUseCase = LoadBookByIdUseCase(booksRepository)
    }

    @Test
    fun `invoke should load book by id`() = runTest {
        // Arrange
        val bookId = 123
        val expectedBook = Book(id = bookId, "", emptyList(), emptyList(), emptyList(), emptyList(),
            emptyList(), false,"Text", mapOf("" to ""), 14)
        `when`(booksRepository.loadBookById(bookId)).thenReturn(expectedBook)


        // Act
        val result = loadBookByIdUseCase(bookId)

        // Assert
        assertEquals(expectedBook, result)
        verify(booksRepository).loadBookById(bookId)
    }
}
