package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.SaveTextFilePathUseCase

@RunWith(MockitoJUnitRunner::class)
class SaveTextFilePathUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var saveTextFilePathUseCase: SaveTextFilePathUseCase

    @Before
    fun setup(){
        saveTextFilePathUseCase = SaveTextFilePathUseCase(booksRepository)
    }
    @Test
    fun `invoke should save text file path`() = runTest {
        // Arrange
        val bookId = 123
        val path = "/path/to/book.txt"

        // Act
        saveTextFilePathUseCase(bookId, path)

        // Assert
        verify(booksRepository).saveTextFilePath(bookId, path)
    }
}
