package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.LoadBooksUseCase

@RunWith(MockitoJUnitRunner::class)
class LoadBooksUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var loadBooksUseCase: LoadBooksUseCase

    @Before
    fun setup(){
        loadBooksUseCase = LoadBooksUseCase(booksRepository)
    }

    @Test
    fun `invoke should load books`() = runTest {
        // Arrange
        val expectedBooks = listOf(
            Book(id = 1, title = "Book 1", emptyList(), emptyList(), emptyList(), emptyList(),
                emptyList(), false,"Text", mapOf("" to ""), 14),
            Book(id = 2, title = "Book 2",  emptyList(), emptyList(), emptyList(), emptyList(),
                emptyList(), false,"Text", mapOf("" to ""), 14)
        )
        `when`(booksRepository.loadBooks()).thenReturn(expectedBooks)

        // Act
        val result = loadBooksUseCase()

        // Assert
        assertEquals(expectedBooks, result)
        verify(booksRepository).loadBooks()
    }
}
