package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.entities.File
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.GetTextFilePathUseCase

@RunWith(MockitoJUnitRunner::class)
class GetTextFilePathUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var getTextFilePathUseCase: GetTextFilePathUseCase

    @Before
    fun setup(){
        getTextFilePathUseCase = GetTextFilePathUseCase(booksRepository)
    }

    @Test
    fun `invoke should return text file path when available`() = runTest {
        // Arrange
        val bookId = 123
        val textPath = "/path/to/book.txt"
        `when`(booksRepository.getFilePaths(bookId)).thenReturn(File(bookId,"","","",textPath))

        // Act
        val result = getTextFilePathUseCase(bookId)

        // Assert
        assertEquals(textPath, result)
        verify(booksRepository).getFilePaths(bookId)
    }

    @Test
    fun `invoke should return null when text file path is not available`() = runTest {
        // Arrange
        val bookId = 123
        val expected = File(bookId,"","","", "")
        `when`(booksRepository.getFilePaths(bookId)).thenReturn(expected)

        // Act
        val result = getTextFilePathUseCase(bookId)

        // Assert
        assertEquals("",result)
        verify(booksRepository).getFilePaths(bookId)
    }
}
