package ru.rvtsk.mypersonallibrary

import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.rvtsk.mypersonallibrary.domain.Result
import ru.rvtsk.mypersonallibrary.domain.entities.Book
import ru.rvtsk.mypersonallibrary.domain.repositories.BooksRepository
import ru.rvtsk.mypersonallibrary.domain.usecases.SearchBooksUseCase

@RunWith(MockitoJUnitRunner::class)
class SearchBooksUseCaseTest {
    @Mock
    private lateinit var booksRepository: BooksRepository

    private lateinit var searchBooksUseCase: SearchBooksUseCase

    @Before
    fun setup(){
        searchBooksUseCase = SearchBooksUseCase(booksRepository)
    }
    @Test
    fun `invoke should search books success`() = runTest {
        // Arrange
        val query = "book"
        val expectedBooks = listOf(
            Book(id = 1, title = "Book 1", emptyList(), emptyList(), emptyList(), emptyList(),
                emptyList(), false,"Text", mapOf("" to ""), 14),
            Book(id = 2, title = "Book 2", emptyList(), emptyList(), emptyList(), emptyList(),
                emptyList(), false,"Text", mapOf("" to ""), 14)
        )
        `when`(booksRepository.searchBooks(query)).thenReturn(Result.Success(expectedBooks))

        // Act
        val result = searchBooksUseCase(query)

        // Assert
        assertEquals(expectedBooks, (result as Result.Success).data)
        verify(booksRepository).searchBooks(query)
    }
    @Test
    fun `invoke should search books error`() = runTest {
        // Arrange
        val query = "book"
        val expected = Exception("")
        `when`(booksRepository.searchBooks(query)).thenReturn(Result.Error(expected))

        // Act
        val result = searchBooksUseCase(query)

        // Assert
        assertEquals(expected, (result as Result.Error).exception)
        verify(booksRepository).searchBooks(query)
    }
}
